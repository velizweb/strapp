Symfony Standard Edition
========================

El proyecto esta relacionado con el Test enviado por Strapp International. Dicho test es un RestFul que va relacionado a servicio de viajes ofrecidos a diferente clientes.

1.- Descargar el Repositorio.
2.- Realizar un composer install
3.- Realizar una copia de app/config/parameters.yml.dist con el nombre de parameters.yml en el cual se configura la onexion a la base de datos que se valla a crear para el proyecto.
4.- Realiar un bin/console doctrine:schema:create para crear las diferentes entidades dentro de la base de datos creada.
5.- Correr el sistema con su servidr virtual con el comando bin/console server:run.


Los endPoint para poder verificar el funcionamiento del api son las siguientes:

Nota: El {id} hace referencia al identificador del cliente. Ejemplo: localhost:8000/customers/1 

Customers o Clientes.
1.- Listar todo los clientes por metodo GET: localhost:8000/customers
2.- Obtener un cliente por metodo GET: localhost:8000/customers/{id}
3.- Actualizar un cliente por metodo PUT: localhost/customers/{id}
4.- Eliminar un cliente por metodo DELETE: localhost/customers/{id}

Travels o viajes.
1.- Listar todo los viajes por metodo GET: localhost:8000/travels
2.- Obtener un viajes por metodo GET: localhost:8000/travels/{id}
3.- Actualizar un viajes por metodo PUT: localhost/travels/{id}
4.- Eliminar un viajes por metodo DELETE: localhost/travels/{id}

Sales o ventas.
1.- Listar todo los ventas por metodo GET: localhost:8000/sales
2.- Obtener un ventas por metodo GET: localhost:8000/sales/{id}
3.- Actualizar un ventas por metodo PUT: localhost/sales/{id}
4.- Eliminar un ventas por metodo DELETE: localhost/sales/{id}



