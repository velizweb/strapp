<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sales
 *
 * @ORM\Table(name="sales", indexes={@ORM\Index(name="customer_id", columns={"customer_id"}), @ORM\Index(name="travel_id", columns={"travel_id"})})
 * @ORM\Entity
 */
class Sales
{
    /**
     * @var integer
     *
     * @ORM\Column(name="adults", type="integer", nullable=false)
     */
    private $adults;

    /**
     * @var integer
     *
     * @ORM\Column(name="children", type="integer", nullable=false)
     */
    private $children;

    /**
     * @var string
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $totalAmount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=false)
     */
    private $updateAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Customers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customers", inversedBy="sales")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;

    /**
     * @var \AppBundle\Entity\Travels
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Travels", inversedBy="sales")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="travel_id", referencedColumnName="id")
     * })
     */
    private $travel;

    /**
     * @return integer
     */
    public function getAdults()
    {
        return $this->adults;
    }

    /**
     * @param integer $adults
     *
     * @return self
     */
    public function setAdults($adults)
    {
        $this->adults = $adults;

        return $this;
    }

    /**
     * @return integer
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param integer $children
     *
     * @return self
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return string
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param string $totalAmount
     *
     * @return self
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTime $updateAt
     *
     * @return self
     */
    public function setUpdateAt(\DateTime $updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param integer $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \AppBundle\Entity\Customers
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param \AppBundle\Entity\Customers $customer
     *
     * @return self
     */
    public function setCustomer(\AppBundle\Entity\Customers $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return \AppBundle\Entity\Travels
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * @param \AppBundle\Entity\Travels $travel
     *
     * @return self
     */
    public function setTravel(\AppBundle\Entity\Travels $travel)
    {
        $this->travel = $travel;

        return $this;
    }
}

