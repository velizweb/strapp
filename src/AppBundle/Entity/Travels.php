<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Travels
 *
 * @ORM\Table(name="travels")
 * @ORM\Entity
 */
class Travels
{
    /**
     * @var string
     *
     * @ORM\Column(name="travel_code", type="string", length=15, nullable=false)
     */
    private $travelCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_tickets", type="integer", nullable=false)
     */
    private $numberTickets;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255, nullable=false)
     */
    private $destination;

    /**
     * @var string
     *
     * @ORM\Column(name="origin", type="string", length=255, nullable=false)
     */
    private $origin;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Sales", mappedBy="travel")
     */
    private $sales;

    public function __construct()
    {
        $this->sales = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getTravelCode()
    {
        return $this->travelCode;
    }

    /**
     * @param string $travelCode
     *
     * @return self
     */
    public function setTravelCode($travelCode)
    {
        $this->travelCode = $travelCode;

        return $this;
    }

    /**
     * @return integer
     */
    public function getNumberTickets()
    {
        return $this->numberTickets;
    }

    /**
     * @param integer $numberTickets
     *
     * @return self
     */
    public function setNumberTickets($numberTickets)
    {
        $this->numberTickets = $numberTickets;

        return $this;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     *
     * @return self
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     *
     * @return self
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     *
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param mixed $sales
     *
     * @return self
     */
    public function setSales($sales)
    {
        $this->sales = $sales;

        return $this;
    }
}

