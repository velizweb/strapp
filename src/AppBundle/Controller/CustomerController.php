<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Customers;

/**
 * Class responsible for controlling everything related to customer.
 * 
 * @Route("/customers")
 */

class CustomerController extends FOSRestController
{
  /**
     * @Rest\Get("/")
     *
     * @return Customers
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        $data = $this->getDoctrine()->getRepository('AppBundle:Customers')->findAll();
        if (is_null($data)) {
            return new View('there are no customers exist', Response::HTTP_NOT_FOUND);
        }
        
        return $data;
    }

    /**
     * @Rest\Get("/{id}")
     *
     * @param Customers $id
     *
     * @return Customers
     */
    public function getAction(Customers $id)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Customers')->find($id);
        if (is_null($data)) {
            return new View('Customer not found.', Response::HTTP_NOT_FOUND);
        }
        
        return $data;
    }

    /**
     * @Rest\Post("/")
     * 
     * @return Customer   
     */
    public function storeAction(Request $request)
    {
        $customer = new Customers;
        $customer->setCedula($request->get('cedula'));
        $customer->setName($request->get('name'));
        $customer->setAddress($request->get('address'));
        $customer->setPhone($request->get('phone'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($customer);
        $em->flush();

        return $customer;
    }

    /**
     * @Rest\Put("/{id}")
     *
     * @param Customers $id
     * 
     * @return Customer   
     */
    public function updateAction(Customers $id, Request $request)
    {
        $customer = $this->getDoctrine()->getRepository('AppBundle:Customers')->find($id);

        if (is_null($customer)) {
            return new View('Customer not found', Response::HTTP_NOT_FOUND);
        }

        $customer->setCedula($request->get('cedula'));
        $customer->setName($request->get('name'));
        $customer->setAddress($request->get('address'));
        $customer->setPhone($request->get('phone'));

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $customer;
    }

    /**
     * @Rest\Delete("/{id}")
     *
     * @param Customers $id
     *
     * @return boolean
     * 
     */
    public function deleteAction(Customers $id)
    {   
      $em = $this->getDoctrine()->getManager();
      $customer = $this->getDoctrine()->getRepository('AppBundle:Customers')->find($id);
      if (is_null($customer)) {
          return false;
      } else {
          $em->remove($customer);
          $em->flush();
          return true;
      }
    }
}
