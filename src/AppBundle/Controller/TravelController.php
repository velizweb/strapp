<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Travels;

/**
 * Class responsible for controlling everything related to travel.
 * 
 * @Route("/travels")
 */
class TravelController extends FOSRestController
{
  /**
     * @Rest\Get("/")
     *
     * @return Travels
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        $data = $this->getDoctrine()->getRepository('AppBundle:Travels')->findAll();
        if (is_null($data)) {
            return new View('there are no travels exist', Response::HTTP_NOT_FOUND);
        }
        
        return $data;
    }

    /**
     * @Rest\Get("/{id}")
     *
     * @param Travels $id
     *
     * @return Travels
     */
    public function getAction(Travels $id)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Travels')->find($id);
        if (is_null($data)) {
            return new View('Travel not found.', Response::HTTP_NOT_FOUND);
        }
        
        return $data;
    }

    /**
     * @Rest\Post("/")
     * 
     * @return Travel   
     */
    public function storeAction(Request $request)
    {
        $travel = new Travels;
        $travel->setTravelCode($request->get('travelCode'));
        $travel->setNumberTickets($request->get('tickets'));
        $travel->setDestination($request->get('destination'));
        $travel->setOrigin($request->get('origin'));
        $travel->setPrice($request->get('price'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($travel);
        $em->flush();

        return $travel;
    }

    /**
     * @Rest\Put("/{id}")
     *
     * @param Travels $id
     * 
     * @return Sale   
     */
    public function updateAction(Travels $id, Request $request)
    {
        $travel = $this->getDoctrine()->getRepository('AppBundle:Travels')->find($id);

        if (is_null($travel)) {
            return new View('Travel not found', Response::HTTP_NOT_FOUND);
        }

        $travel->setTravelCode($request->get('travelCode'));
        $travel->setNumberTickets($request->get('tickets'));
        $travel->setDestination($request->get('destination'));
        $travel->setOrigin($request->get('origin'));
        $travel->setPrice($request->get('price'));

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $travel;
    }

    /**
     * @Rest\Delete("/{id}")
     *
     * @param Travels $id
     *
     * @return boolean
     * 
     */
    public function deleteAction(Travels $id)
    {   
        $em = $this->getDoctrine()->getManager();
        $travel = $this->getDoctrine()->getRepository('AppBundle:Travels')->find($id);
        if (is_null($travel)) {
            return false;
        } else {
            $em->remove($travel);
            $em->flush();
            return true;
        }
    }
}
