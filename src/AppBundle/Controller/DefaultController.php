<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Sales;

/**
 * Class responsible for controlling everything related to sales.
 * 
 * @Route("/sales")
 */
class DefaultController extends FOSRestController
{
    /**
     * @Rest\Get("/")
     *
     * @return Sales
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        $data = $this->getDoctrine()->getRepository('AppBundle:Sales')->findAll();
        if (is_null($data)) {
            return new View('there are no sales exist', Response::HTTP_NOT_FOUND);
        }
        
        return $data;
    }

    /**
     * @Rest\Get("/{id}")
     *
     * @param Sales $id
     *
     * @return Sales
     */
    public function getAction(Sales $id)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Sales')->find($id);
        if (is_null($data)) {
            return new View('Sale not found.', Response::HTTP_NOT_FOUND);
        }
        
        return $data;
    }

    /**
     * @Rest\Post("/")
     * 
     * @return Sale   
     */
    public function storeAction(Request $request)
    {
        $sale = new Sales;
        $customer = $this->getDoctrine()->getRepository('AppBundle:Customers')->find($request->get('customer'));
        $travel = $this->getDoctrine()->getRepository('AppBundle:Travels')->find($request->get('travel'));
        $sale->setCustomer($customer);
        $sale->setTravel($travel);
        $sale->setTotalAmount($request->get('totalAmount'));
        $sale->setAdults($request->get('adults'));
        $sale->setChildren($request->get('children'));
        $sale->setStatus(1);
        $sale->setCreatedAt(new \Datetime('now'));
        $sale->setUpdateAt(new \Datetime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($sale);
        $em->flush();

        return $sale;
    }

    /**
     * @Rest\Put("/{id}")
     *
     * @param Sales $id
     * 
     * @return Sale   
     */
    public function updateAction(Sales $id, Request $request)
    {
        $sale = $this->getDoctrine()->getRepository('AppBundle:Sales')->find($id);

        if (is_null($sale)) {
            return new View('Sale not found', Response::HTTP_NOT_FOUND);
        }

        $customer = $this->getDoctrine()->getRepository('AppBundle:Customers')->find($request->get('customer'));
        $travel = $this->getDoctrine()->getRepository('AppBundle:Travels')->find($request->get('travel'));
        $sale->setCustomer($customer);
        $sale->setTravel($travel);
        $sale->setTotalAmount($request->get('totalAmount'));
        $sale->setAdults($request->get('adults'));
        $sale->setChildren($request->get('children'));
        $sale->setStatus(1);
        $sale->setCreatedAt(new \Datetime('now'));
        $sale->setUpdateAt(new \Datetime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $sale;
    }

    /**
     * @Rest\Delete("/{id}")
     *
     * @param $sales $id
     *
     * @return boolean
     * 
     */
    public function deleteAction(Sales $id)
    {   
        $em = $this->getDoctrine()->getManager();
        $sale = $this->getDoctrine()->getRepository('AppBundle:Sales')->find($id);
        if (is_null($sale)) {
            return false;
        } else {
            $em->remove($sale);
            $em->flush();
            return true;
        }
    }
}
